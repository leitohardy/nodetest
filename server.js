let http = require('http');
let fs = require('fs');

function myRequest(req, res) {
	// console.log(req.url);
	// console.log(req.method);
	// console.log(req.headers.host);

	res.writeHead(200, { 'Content-Type' : 'html'});

	fs.readFile('./main.html', null, function(error, data) {
		if (error) {
			res.writeHead(404);
		} else {
			res.write(data);
		}

		res.end();
	});


	
}

let server = http.createServer(myRequest);

server.listen(8080, function() {
	console.log('работает');
});