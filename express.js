let express = require('express');
let app = express();
let port = 8080;
let expressLayouts = require('express-ejs-layouts');
let path = require('path');
let bodyParser = require('body-parser');


app.use(bodyParser.urlencoded({
  extended: true
}));

// routing file
let router = require('./router');

app.listen(port, function(){
	console.log('express is started');
});

// templating // layout - start point
app.set('view engine' , 'ejs');
app.use(expressLayouts);
app.set('views', path.join(__dirname, '/public')); // change path

// routing
app.use('/', router);

// use static files // css, img
app.use(express.static(__dirname + '/public'));

