let express = require('express');
let fs = require('fs');
let router = express.Router();
let path = require('path'); 

router.get('/', function(req, res){
	res.render('home');
});


router.get('/page', function(req, res){
	res.render('page');
});

router.get('/about', function(req, res){
	
	let users = [
		{ name: 'Leito', avatar : 'http://placekitten.com/300/300'},
		{ name: 'Hardy', avatar : 'http://placekitten.com/400/400'},
		{ name: 'John', avatar : 'http://placekitten.com/800/800'},
		{ name: 'Rocky', avatar : 'http://placekitten.com/740/740'},
		{ name: 'Micky', avatar : 'http://placekitten.com/500/500'}
	];

	res.render('about', { users: users });


});

router.post('/about', function(req, res){
	res.send(`Hello mr ${req.body.name}`);
});


module.exports =  router;